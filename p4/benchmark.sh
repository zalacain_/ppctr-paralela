
rm tiempos.txt
make build

echo "TIEMPOS PARALELO">>tiempos.txt
for i in `seq 1 1 100`;
do
  ./build/mandelbrot    >> tiempos.txt
done

echo "TIEMPOS RUNTIME">>tiempos.txt
for i in `seq 1 1 100`;
do
  ./build/mandelbrotRun    >> tiempos.txt
done


echo "TIEMPOS SECUENCIAL CRITICA">>tiempos.txt
for i in `seq 1 1 100`;
do
  ./build/mandelbrotSec  >> tiempos.txt
done


echo "TIEMPOS VARIABLES PRIVADAS">>tiempos.txt
for i in `seq 1 1 100`;
do
  ./build/mandelbrotVar    >> tiempos.txt
done



echo "TIEMPOS PROGRAMA SECUENCIAL">>tiempos.txt
for i in `seq 1 1 100`;
do
  ./build/mandelbrotSecuencial    >> tiempos.txt
done
