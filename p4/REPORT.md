# Practica 4 mandelbrot

Autor Juan Zalacain Garcia 

Fecha 15/12/2018

## Prefacio


LEER README

Esta practica es interesante debido a que ejecutas el mismo programa con ciertas modificaciones y puedes ver las diferencias de tiempos .

Al ser esta la ultima practica voy a dar una opinion general sobre lo que me ha parecido este bloque,la parte de tener que subir el codigo a gitlab a traves de la terminal y el enlazado con el directorio de primeras me parecio algo poco util y complicado en relacion  a las ventajas que veia,pero al final me ha parecido util y bastante sencillo ,me gusta que queden registrados los cambios y ademas en el caso que se te corrompa algun archivo lo puedes bajar de tu directorio.


## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VMware:
  - Ubuntu Desktop 18.10 64bits actualizado
  - 1,9 GiB  RAM
  -Entorno grafico
  - 8 Cores
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i5-8300H CPU @ 2.30GHz (8 CPUs), ~2.3GHz
  - CPU 4 cores: 1 socket, 2 threads/core, 4 cores/socket.
  - CPU @2300 MHz (fijada con cpufreq usermode)
  - CPU: L2: 2MiB, L3: 8MiB
  - 8 GiB RAM
  - Trabajo y benchmarks sobre SSD
  - 50 procesos en promedio antes de ejecuciones

## 2. Diseño e Implementación del Software




En esta practica ,en  la primera parte partimos del codigo secuencial y hay que convertirlo en paralelo mediante directivas OpenMP

Para ello debo usar la directiva #pragma omp parallel poniendo como privado los indices de los for ,las coordenadas y c, el resto es compartido de base.

```
#pragma omp parallel private(i,j,x,y,c)
```



Ahora paso a paralelizar los for anidados mediante #pragma omp for, despues faltaria por paralelizar r,g y b para que solo acceda un hilo,esto lo hare mediante #pragma omp single 


```
#pragma omp single
          {
            r = (int *)malloc(n * n * sizeof(int));
            g = (int *)malloc(n * n * sizeof(int));
            b = (int *)malloc(n * n * sizeof(int));
          }

```
Ya estaria paralelizado


En la segunda parte de la practica hay una parte del programa en el cual se modifica c_max por  los hilos  y esta a su vez es compartida por lo que hay protegerla mediante una seccion critica.Hay que hacerlo de 4 formas(todas estas formas parten del codigo modificado en el apartado 1):

La primera forma mediante directivas omp ,para conseguirlo bastaria con añadir en dicha seccion la directiva #pragma omp critical


```
#pragma omp critical
{
    if (c_max < count[i + j * n]) {
       c_max = count[i + j * n];
    }
}

```



En la segunda forma se hace mediante funciones runtime,para ello necesitamos crear un lock fuera de la seccion paralela  e inicializarlo a no bloqueado

```
omp_lock_t mutex;
omp_init_lock(&mutex);
```
Despues en la seccion critica debemos tomar el lock y una vex acabada la seccion lo liberamos
```

  omp_set_lock(&mutex);
      if (c_max < count[i + j * n]) {
      c_max = count[i + j * n];
  }
  omp_unset_lock(&mutex);

```



Para la tercera  forma se hace secuencial,para ello  el acceso al for que contiene la seccion critica solo lo puede hacer un thread,esto lo logramos mediante un #pragma omp single en vez del #pragma omp for y el resto no se modifica 

```

#pragma omp single
{
   for(int i=0;i<omp_get_num_threads();i++){
      if (resultadosTMP[i] > c_max) {
      c_max=resultadosTMP[i];
       }
   }
}

```



Para finalizar,la 4 y ultima forma sera mediante variables privadas a cada thread y seleccion del maximo de todas ellas 
Para esta ultima parte se debe crear un array fuera de la seccion paralela el cual contendra doubles los cuales representan los resultados temporales,el tamaño de dicho array sera el numero de threads y lo inicializaremos a 0

```
double resultadosTMP [omp_get_max_threads()];
        for(int i=0;i<omp_get_max_threads();i++){
          resultadosTMP[i]=0;
        }
```


Ahora llenamos el array creado en la seccion paralela donde cada hilo mete su resultado en la posicion del array la cual es el numero del hilo 


```
#pragma omp for
for (j = 0; j < n; j++) {
     for (i = 0; i < n; i++) {
         if (resultadosTMP[omp_get_thread_num()] < count[i + j * n]) {
            resultadosTMP[omp_get_thread_num()]= count[i + j * n];
          }
       }
 }
```

Despues en la region critica recorremos eñ array de resultados y guardamos en cmax el mayor valor del array
```
 #pragma omp single
 {
    for(int i=0;i<omp_get_num_threads();i++){
         if (resultadosTMP[i] > c_max) {
              c_max=resultadosTMP[i];
          }
     }
}

```




## 3. Metodología y desarrollo de las pruebas realizadas
Para realizar el benchmark de esta practica,al tratarse de 4 codigos diferentes he tenido que modificar el make file para que compile los nuevos archivos.
Una vez modificado el make file he creado un pequeño script el cual ejecuta el programa 100 veces y guarda los resultados en un archivo,posteriormente mediante excel calculo las medias de los tiempos
y las graficas. Este script se llama benchmark.sh.
La ejecucion se realizo con el portatil conectado a la corriente ya que el tiempo variaba dependiendo de estar conectado o no.

Las ejecuciones estan hechas con un tamaño n=800


Tiempo de ejecucion secuencial:0.55385557

![](images/secuencialmandel.png)


Tiempo de ejecucion paralelo(a):0.30118093

![](images/paralelomandel.png)


Tiempo de ejecucion Runtime(b):0.31416301

![](images/runtimemandel.png)



Tiempo de ejecucion Secuencial critico(c):0.22466793

![](images/secuencialcritico.png)


Tiempo de ejecucion Variables privadas(d):0.34333016

![](images/varprivad.png)



SPEEDUP paralelo=1.838946344

SPEEDUP	runtime=1.762956021

SPEEDUP	secuencial region critica=2.465218645

SPEEDUP	variables privadas=1.613186473





## 4. Discusión

1.  Explica brevemente la funcion que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado (solo las que no hayas explicado en practicas anteriores).

#pragma omp critical

Esta directiva indica que todos los threads pueden acceder al bloque pero solo uno cada vez

#omp_lock_t mutex
Con ello creo un lock llamado mutex


#omp_init_lock(&mutex)
Con ello inicializo el lock a no bloqueado



#omp_set_lock(&mutex)
Coge el control del lock en el caso de que este libre,si no tendra que esperar.


#omp_unset_lock(&mutex)
Libera el control del lock



#omp_get_thread_num()
Devuelve el numero del thread actual



#omp_get_num_threads()
Devuelve el numero de threads activos en dicha region paralela


#omp_get_max_threads()
Devuelve el numero maximo de threads que el sistema soporta



2.  Etiqueta todas las variables y explica por que le has asignado esa etiqueta.

#pragma omp parallel private(i,j,x,y,c)

Aqui puse las variables  i,j,x,y,c como privadas  ya que la i y la j son los indices de los for que deben ser unicos por cada hilo asi como las coordenadas.
El resto de variables son compartidas de forma predeterminada.



3.  Explica brevemente como se realiza el paralelismo en este programa, indicando las partes que son secuenciales y las paralelas.

La explicacion de como se realiza el paralelismo se encuentra en la explicacion del desarrollo del codigo.

El programa es secuencial  hasta que creamos la region paralela(pragma omp parallel),tambien son secuenciales las partes donde esta la directiva pragma omp  single ya que solo un hilo realizara esta seccion.

El paralelismo se realiza mediante las directivas #pragma omp for y #pragma omp critical.


4.  Bonus optativo: utiliza alguna herramienta de profiling (eg. gprof) para determinar que funciones deben ser optimizadas y/o paralelizadas. Explica como lo has usado
y que decisiones has tomado. De cara a esta practica no pasa nada porque lo hagas una vez hayas paralelizado.



















