# Practica 1  Multithreading

Autor Juan Zalacain Garcia 

Fecha 15/12/2018

## Prefacio


LEER README

Esta practica me ha parecido la mas complicada de todas,hasta el punto de no saber como hacer el logger despues de muchos intentos,he comentado la parte del logger para que asi al menos la parte basica funcione y compile de manera correcta.


## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VMware:
  - Ubuntu Desktop 18.10 64bits actualizado
  - 1,9 GiB  RAM
  -Entorno grafico
  - 8 Cores
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i5-8300H CPU @ 2.30GHz (8 CPUs), ~2.3GHz
  - CPU 4 cores: 1 socket, 2 threads/core, 4 cores/socket.
  - CPU @2300 MHz (fijada con cpufreq usermode)
  - CPU: L2: 2MiB, L3: 8MiB
  - 8 GiB RAM
  - Trabajo y benchmarks sobre SSD
  - 50 procesos en promedio antes de ejecuciones

## 2. Diseño e Implementación del Software









## 3. Metodología y desarrollo de las pruebas realizadas



## 4. Discusión


















