#include <stdio.h>
#include <thread>
#include <string.h>
#include <mutex>
#include <condition_variable>

std::mutex g_m;
std::condition_variable g_c;
bool g_ready = false;


double resultadoTemp;
double resultadoFinal;




void recogeResultados(){
  puts("PALO3");
  g_ready=false;
  std::unique_lock<std::mutex> ulk(g_m);// own mutex
  //g_c.wait(ulk, []{ return g_ready;});
  // ... process (own mutex)
  resultadoFinal+=resultadoTemp;
  printf("R:%f",resultadoFinal);
  ulk.unlock();
  // free mutex
}

double  sum(double lista[],int inicio,int fin){
  double total=0;
    for(int i=inicio;i<fin;i++){
      printf("+%d",(int)lista[i]);
      total+=lista[i];
    }
    return total;
}

double xo(double lista[],int inicio ,int fin){
  double total=0;
  for(int i=inicio;i<fin;i++){
    total+=lista[i];
  }
  return total;
}


void sum_xor(char *modo,int inicio,int fin,double *array){
  if(strcmp(modo,"sum") == 0){
    puts("\nSUMA:");
    resultadoTemp=sum(array,inicio,fin);
    printf("= %d\n",(int)resultadoTemp);
    //g_c.notify_one();
    //std::lock_guard<std::mutex> lock(g_m);
    //g_ready = true;
    // signal
  }
  if(strcmp(modo,"xor") == 0){
    resultadoTemp=xo(array,inicio,fin);
    printf("XOR: %f",resultadoTemp);
    //std::lock_guard<std::mutex> lock(g_m);
    //g_ready = true;
  }
 }







int  main (int argc,char* argv[]){
  int numEle=atoi(argv[1]);
  int  numThreads=atoi(argv[4]);
  char *modo=argv[2];
  if(numThreads==0){
    numThreads=1;
  }

  if(numThreads>10){
    printf("Error,numero maximo de hilos excedido");
    exit(1);
  }
  if(strcmp(modo,"xor") != 0){
    if(strcmp(modo,"sum")!=0){
      printf("Error,modo incorrecto");
      exit(1);
    }
  }
  puts("PARAMETROS");
  printf("Numero de elementos:%d\n",numEle);
  printf("Numero de threads: %d\n",numThreads);



   double* array= (double*)malloc(numEle*sizeof(double));
   for(int i=0;i<numEle;i++){
     array[i]=(double)i;
   }
 //calculo el tamaño de cada elemento
   int tamanoThread=numEle/numThreads;
   double  resto=numEle%numThreads;
   std::thread hilos[numThreads];


   std::thread logger=std::thread(recogeResultados);
   logger.join();


    int inicio=0;
    int fin=tamanoThread+resto;
    puts("Creo los hilos\n");
    for(int i=0;i<numThreads;i++){
          printf("\nHILO[%d]",i);
          hilos[i]=std::thread(sum_xor,modo,inicio,fin,array);
          inicio=fin;
          fin+=tamanoThread;
    }
    for(int i=0;i<numThreads;i++){
          hilos[i].join();
    }
    printf(" Resultado:%f\n",resultadoFinal);
}
