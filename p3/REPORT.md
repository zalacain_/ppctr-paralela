# Practica 3 videotask

Autor Juan Zalacain Garcia 

Fecha 15/12/2018

## Prefacio


LEER README



Esta practica me ha parecido mas entretenida de realizar que la anterior ,ademas me ha resultado interesante la forma de usar el buffer para no sobrecargar la ram en caso de archivos grandes y el uso de las tasks


## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.

- Ejecución sobre Máquina Virtual VMware:
  - Ubuntu Desktop 18.10 64bits actualizado
  - 1,9 GiB  RAM
  -Entorno grafico
  - 8 Cores
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i5-8300H CPU @ 2.30GHz (8 CPUs), ~2.3GHz
  - CPU 4 cores: 1 socket, 2 threads/core, 4 cores/socket.
  - CPU @2300 MHz (fijada con cpufreq usermode)
  - CPU: L2: 2MiB, L3: 8MiB
  - 8 GiB RAM
  - Trabajo y benchmarks sobre SSD
  - 50 procesos en promedio antes de ejecuciones

## 2. Diseño e Implementación del Software




En esta practica partimos del codigo secuencial y hay que convertirlo en paralelo mediante directivas OpenMP y task

La region a paralelizar sera la que contiene los bucles con fgauss y fwrite que son las que mas peso llevan en el programa (lectura y escritura

Para paralelizar dicha zona del programa comenzaremos con la directiva #pragma omp parallel ,despues de ello ,como solo un hilo sera el encargado de crear las tareas que ejecutaran los threads debemos poner la directiva #pragma omp single para que solo un hilo acceda.
Ahora debemos separar fgauss y fwrite,ya que este ultimo debe esperar a que fgauss llene el buffer con las imagenes y despues ya podra fwrite volcarlas.Para ello utilizamos #pragma omp task para fgauss con el indice i privado para que no lo modifiquen.

```
#pragma omp task firstprivate(i)
                fgauss (pixels[i], filtered[i], height, width);
```


Una vez que el buffer se haya llenado se debe esperar a que las tasks de fgauss acaben y ahora empezaremos a escribir con fwrite el contenido del buffer.
Una vez escrito el contenido del buffer se debe reiniciar el indice del buffer para poder volver a llenarlo con las 8 siguientes.

```
if(i==seq-1){
      	   #pragma omp taskwait
           for(int w=0;w<=seq-1;w++){
              fwrite(filtered[w], (height+2) * (width + 2) * sizeof(int), 1, out);
	      puts("Escribo");
	   }
}
i++;
if(i==seq){
      i=0;
 }
```

Ademas debido a que hasta este punto el codigo solo escribe cuando el buffer se llena, hay que tener en cuenta cuando el buffer no esta lleno pero ya acabo,para ello fuera del do while escribimos el resto del   buffer .

```
#pragma omp taskwait
for(int w=0;w<i-1;w++){
      fwrite(filtered[w], (height+2) * (width + 2) * sizeof(int), 1, out);
      puts("Escribo");
}
```

Ahora ya es capaz de escribir el contenido aunque el buffer no este lleno 


## 3. Metodología y desarrollo de las pruebas realizadas

Para el benchmarkin he creado un script llamado benchmark.sh el cual ejecuta 100 veces el programa y anota los tiempos,tambien tuve que modificar el makefile para que compilara tanto el progama secuencial como el paralelo.El script tiene el codigo para ejecutar 100 veces ambos programas,se puede comentar y descomentar la parte que se quiera para tan solo ejecutar una de ellas.Despues los tiempos son volcados en un archivo txt llamado tiempos mediante el cual posteriormente y con el uso de excel calculo las graficas y los tiempos medios de ejecucion.


Durante la ejecuion de las pruebas me di cuenta que si mi portatil no estaba enchufado a la corriente los tiempos ascendian mucho por lo que todas las pruebas fueron ejecutadas con el ordenador conectado a la corriente.


Media tiempo de ejecucion paralelo:0.80386519

Media tiempo ejecucion secuencial:2.27069141


Speedup=2,825




GRAFICA TIEMPOS PARALELOS

![](images/tiemposPar.png)





GRAFICA TIEMPOS SECUENCIALES


![](images/tiemposSec.png)




COMPARATIVA GRAFICAS



![](images/tiemposComp.png)







## 4. Discusión





1.  Explica  que  funcionalidad  has  tenido  que  añadir/modificar  sobre  el  codigo  ori-
ginal. Explica si has encontrado algun fallo en la funcionalidad (“lo que hace el
programa”, independientemente al paralelismo) y como lo has corregido. Atencion:
encontrar el fallo es algo “para nota”, por lo que no te deber ́ıas preocupar si no lo
encuentras.

La respuesta a esta pregunta queda respondida durante la explicacion del desarrollo del codigo en el apartado 1

2.  Explica brevemente la funcion que desarrolla cada una de las directivas y funciones
de  OpenMP  que  has  utilizado,  salvo  las  que  ya  hayas  explicado  en  la  practica
anterior (por ejemplo parallel).

#pragma omp single
Esta directiva indica que el bloque solo lo va a ejecutar un unico thread,este thread sera el primero en llegar.


#pragma omp task
Genera tareas de forma explicita,es un unico hilo(single) el que las genera.Estas tareas se meten en una cola y los threads al quedar libres
van cogiendo de esa cola las tareas 


#pragma omp taskwait
Esta directiva hace de barrera ,espera a que todas las tareas generadas por el single finalizen.





3. Etiqueta todas las variables y explica por que le has asignado esa etiqueta.
	#pragma omp task firstprivate(i)
	En esta directiva puse la i como privada inicializandola al valor del master,esto lo hice ya que los demas threads la usan tambien y la cambian por lo que afectaria a la  tarea .

4.  Explica como se consigue el paralelismo en este programa (aplicable sobre la fun-
cion main).
La respuesta a esta pregunta queda respondida en el apartado donde se explica el codigo ,el 1
El paralelismo se consigue mediante las directivas task las cuales se encargan de crear las tareas que son ejecutadas por los hilos 



5.  Calcula la ganancia (speedup) obtenido con la paralelizacion y explica los resulta-
dos obtenidos.


La ganancia es  de 2,825 como puede verse en el apartado del benchmarking ,en los resultados obtenidos se puede observar claramente como el programa secuencial es mas lento que el paralelo y mas irregular en tiempos 



6.  ¿Se te ocurre alguna optimizacion sobre la funciOn fgauss? Explica que has hecho
y que ganancia supone con respecto a la version paralela anterior







