# Practica 2 matmul

Autor Juan Zalacain Garcia 

Fecha 15/12/2018

## Prefacio

LEER README

Haciendo una retrospectica de todas las practicas esta quizas fue la que menos me gusto,no tanto por el codigo si no mas por el informe,son demasiadas ejecuciones y mucho tiempo empleado en ejecutar y esperar .

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.


- Ejecución sobre Máquina Virtual VMware:
  - Ubuntu Desktop 18.10 64bits actualizado
  - 1,9 GiB  RAM
  -Entorno grafico
  - 8 Cores
- Host (`lscpu, cpufreq, lshw, htop`):
  - CPU Intel(R) Core(TM) i5-8300H CPU @ 2.30GHz (8 CPUs), ~2.3GHz
  - CPU 4 cores: 1 socket, 2 threads/core, 4 cores/socket.
  - CPU @2300 MHz (fijada con cpufreq usermode)
  - CPU: L2: 2MiB, L3: 8MiB
  - 8 GiB RAM
  - Trabajo y benchmarks sobre SSD
  - 50 procesos en promedio antes de ejecuciones


## 2. Diseño e Implementación del Software




En esta practica partimos del codigo secuencial y hay que convertirlo en paralelo mediante directivas OpenMP

La primera parte del codigo en paralelizar sera la funcion matmul la cual multiplica matrices.
	Mediante la directiva #pragma omp parallel colocada justo despues de inicializar los indices de los for ,poniendo como privados los indices pero como compartidos
	las dimensiones de las matrices y el tamaño del bloque 
```
	#pragma omp parallel  private(i,j,k) shared (A,B,C,dim)
```
 	Para finalizar la paralelizacion de este metodo debemos parelelizar los for,cada uno de los dos for que anidan otros for mediante la directiva #pragma omp for
	 

 	Ademas esta funcion tiene que ser capaz de contar el tiempo de ejecucion y para ello utilizo el metodo omp_get_wtime() al inicio y al final para asi mediante la resta de ambos poder calcular el tiempo final de ejecucion y mostrarlo por pantalla.


La siguiente funcion a paralelizar es matmul_sup y se hace de forma similar a matmul.
La primera parte es crear la seccion paralela mediante #pragma omp parallel y despues los for mediante #pragma omp for 
```
	#pragma omp parallel  private(i,j,k) shared (A,B,C,dim)
```


Despues paralelizo cada for mediante #pragma omp for schedule(guided/static/dynamic)








## 3. Metodología y desarrollo de las pruebas realizadas

Para realizar estas pruebas he creado un script llamado benchmark.sh el cual ejecuta el programa     10          veces calculando para cada uno los tiempos y volcando los resultados en un archivo txt llamado resultados txt.

Para medir las diferentes operaciones simplemente comento y descomento las que no quiera medir en la main del codigo y ejecuto el script.

La matriz a calcular tiene una dimension de 1233 ,en mi ordenador suele tardan unos 10 segundos en secuencial.



Tiempo medio de ejecucion  de matmul paralelo:4.7473744

![](images/matmulpar.png)


Tiempo medio de ejecucion de matmul secuencial:10.0040311

![](images/matmulsec.png)

SPEEDUP MATMUL:2.107



Tiempo medio de ejecucion de matmul_sup paralelo(guided):1.7122091

Tiempo medio de ejecucion de matmul_sup paralelo(static):1.7352291

Tiempo medio de ejecucion de matmul_sup paralelo(dynamic):1.5583414




Tiempo medio de ejecucion de matmul_sup paralelo sin schedule:1.4547018

![](images/matmulsupPar.png)


Tiempo medio de ejecucion de matmul_sup secuencial:4.2955956

![](images/matmulsupsec.png)


El metodo matmul_sup paralelo tiene una ganancia de 2.9528

## 4. Discusión

EJERCICIO 1



1.
Explica brevemente la función que desarrolla cada una de las directivas y funciones
de OpenMP que has utilizado.




#omp_get_wtime();
Esta directiva devuelve el tiempo de ejecucion en segundos hasta ese punto del programa en cuestion



#pragma omp parallel
Esta directiva define una region paralela ,un bloque, que sera ejecutado por varios threads en paralelo y al final del bloque solo continua un unico thread,el maestro


#pragma omp for

Esta directiva especifica que iteraciones del bucle for deben ejecutarse en paralelo(debe estar dentro de una region paralela).







2.
Etiqueta (OpenMP) todas las variables y explica por qué le has asignado esa etiqueta.

#pragma omp parallel  private(i,j,k) shared (A,B,C,dim)
En esta directiva puse las variables  i,j,k como privadas ya que se tratan de los indices de los bucles for y deben ser unicas para cada hilo sin compartirse
Las variables A,B,C,dim las puse como compartidas ya que son usadas por todos los hilos de forma unica





3.
Explica cómo se reparte el trabajo entre los threads del equipo en el código paralelo.
El trabajo del programa se reparte de forma paralela en los bucles for,cada thread coge una iteracion del for para ejecutarla(el superior).






4.
Calcula la ganancia (speedup) obtenido con la paralelización.

Tiempo medio de ejecucion  de matmul paralelo:4.7473744

Tiempo medio de ejecucion de matmul secuencial:10.0040311

SPEEDUP MATMUL:2.107 
Esta ganancia indica que el programa paralelo es 2,1 veces mas rapido








EJERCICIO 2


1.
¿Qué diferencias observas con el ejercicio anterior en cuanto a la ganancia obtenida sin la clausula schedule? 
Explica con detalle a qué se debe esta diferencia.



Tiempo medio de ejecucion de matmul_sup paralelo sin schedule:1.4547018

Tiempo medio de ejecucion  de matmul paralelo:4.7473744

Ganancia de matmulsup:3,27 

Que el tiempo de ejecucion de matmul_sup sea menor que el de matmul es debido a que este metodo solo multiplica la matriz superior y no toda la matriz como hace matmul.



2.
¿Cuál de los tres algoritmos de equilibrio de carga obtiene mejor resultado? Explica por qué ocurre esto, en función de cómo se reparte la carga de trabajo y las
operaciones que tiene que realizar cada thread.


Tiempo medio de ejecucion de matmul_sup paralelo(guided):1.7122091

Tiempo medio de ejecucion de matmul_sup paralelo(static):1.7352291

Tiempo medio de ejecucion de matmul_sup paralelo(dynamic):1.5583414

El algoritmo que obtiene mejor resultado es el schedule(dynamic) ,esto es debido a que asigna las tareas dinamicamente a los threads segun van acabando otras tareas

En cambio el static asigna una carga de trabajo fija para todos y si un thread ya acabo su parte y aun queda mas ,este quedara esperando.




3.
Para el algoritmo static piensa cuál será el tamaño del bloque óptimo, en función de cómo se reparte la carga de trabajo.

El tamaño de bloque obtimo entiendo que es el mayor



4.
Para el algoritmo dynamic determina experimentalmente el tamaño del bloque óptimo. Para ello mide el tiempo de respuesta y speedup obtenidos para al menos

10 tamaños de bloque diferentes. Presenta una tabla resumen de los resultados y explicar detalladamente estos resultados.


![](images/tablaDynamic.png)



El tamaño  de bloque optimo sera el menor de los tamaños,en la tabla se aprecia como cada vez que aumento los tamaños emperoan los tiempos.Asi habra mas dinamismo en la asignacion de tareas 





5.
Explica los resultados del algoritmo guided , en función del reparto de carga de trabajo que realiza.

Su tamaño de bloque mejor es uno pequeño,es similar al dynamic pero añadira  ligeramente mas overflow que el dynamic .

Sus tamaños de bloque decrecen.

EJERCICIO 3




1.
¿Cómo se comportará? Compáralo con el ejercicio anterior.

Es parecido al anterior(obviamente sin paralelizar)ya que multiplica solo la matriz inferior.


2.
¿Cuál crees que es el mejor algoritmo de equilibrio en este caso? Explica las diferencias que ves con el ejercicio anterior.

Para este caso creo que el mejor algormito es el dynamic para asi repartir mejor el trabajo,al tratarse de un codigo similar y siguiendo los resultados de los tiempos anteriores he elegido dynamic




3.
¿Cuál es el peor algoritmo para este caso?


El peor algortmo es el static ya que es similar a matmul_sup por lo que el mejor algoritmo de ese lo sera para este.
 




